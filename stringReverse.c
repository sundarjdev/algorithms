#include <stdio.h>
#include <string.h>

void stringReverse(char *a)
{
    int i, j;
    for (i = 0, j = strlen(a)-1; i < strlen(a)/2; i++, j--) {
        char temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}

int main(void)
{
    char a[] = "SundarDev";
    char b[] = "VaniSundar";

    stringReverse(a);
    stringReverse(b);

    printf("Reverse(a[%d]): %s\n", strlen(a), a);
    printf("Reverse(b[%d]): %s\n", strlen(b), b);

    return 0;
}
