/*
 * Given a sorted array in ascending order, which is also
 * rotated internally somewhere. Find the point of rotation
 */

#include <stdio.h>

int find_point_of_rotation(int *a, int imin, int imax)
{
    if (imin >= imax)
        return 0;

    int mid = (imin + imax) / 2;
    if (a[mid] > a[mid + 1])
        return (mid + 1);
    else if (a[mid] > a[imin])
        return find_point_of_rotation(a, mid, imax);
    else
        return find_point_of_rotation(a, imin, mid);
}

int main(void)
{
    int a[10] = {5, 6, 7, 8, 9, 10, 1, 2, 3, 4}; // 6
    int b[6] = {1, 2, 3, 4, 5, 6 }; // 0
    int c[3] = {3, 2, 1}; // 0
    int d[9] = {15, 20, 25, 30, 35, 40, 45, 10, 12}; // 7

    printf("a: %d\n", find_point_of_rotation(a, 0, (sizeof(a)/sizeof(int)) - 1));
    printf("b: %d\n", find_point_of_rotation(b, 0, (sizeof(b)/sizeof(int)) - 1));
    printf("c: %d\n", find_point_of_rotation(c, 0, (sizeof(c)/sizeof(int)) - 1));
    printf("d: %d\n", find_point_of_rotation(d, 0, (sizeof(d)/sizeof(int)) - 1));

    return 0;
}
