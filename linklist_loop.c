#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

void push(struct node **headref, int val)
{
    struct node *node = malloc(sizeof(struct node));
    node->val = val;
    node->next = *headref;
    *headref = node;
}

void printlist(struct node *head)
{
    while (head != NULL) {
        printf("%d-->", head->val);
        head = head->next;
    }
    printf("\n");
}

int main(void)
{
    struct node *head = NULL;
    struct node *last = NULL;
    struct node *loop = NULL;
    
    for (int i = 0; i < 1; i++) {
        push(&head, i);
        if (i == 0)
            last = head;

        if (i == 0)
            loop = head;
    }

    printlist(head);

    last->next = loop; /* create loop */
    printf("Loop created at: %d\n", loop->val);

    struct node *p1 = head, *p2 = head;

    p1 = p1->next;
    p2 = p2->next->next;
    while (p1 && p2 && p1 != p2) {
        p1 = p1->next;
        p2 = p2->next->next;
    }

    p1 = head;
    while (p1 != p2) {
        p1 = p1->next;
        p2 = p2->next;
    }

    printf("Loop at %d\n", p1->val);

    return 0;
}
