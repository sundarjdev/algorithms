/*
 * This program was intended to find a duplicate number in a un-sorted array of
 * numbers starting from 1...N
 */
#include <stdio.h>

int main(void)
{
    int N = 9;
    int a[] = {6, 4, 5, 2, 3, 9, 7, 1, 8, 9};
    int xor = 0x0;

    for (int i = 1; i <= N; i++)
        xor ^= i;
    printf("xor: %d\n", xor);

    for (int i = 0; i < sizeof(a)/sizeof(int); i++)
        xor ^= a[i];

    printf("duplicate: %d\n", xor);

    return 0;
}
