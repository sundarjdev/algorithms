/*
 * Given a NxN matrix with 0s and 1s. Set every row that contains a 0 to all 0s
 * and set every column that contains a 0 to all 0s
 */
#include <stdio.h>
#define N 5

#define bool char 

bool A[N][N] = {
  /*       0, 1, 1, 0, 0 */
  /* 0 */ {1, 1, 1, 1, 0},
  /* 1 */ {1, 1, 1, 1, 1},
  /* 0 */ {1, 1, 1, 0, 1},
  /* 1 */ {1, 1, 1, 1, 1},
  /* 0 */ {0, 1, 1, 1, 0}};

bool row[N] = {1, 1, 1, 1, 1};
bool col[N] = {1, 1, 1, 1, 1};

int main(void)
{
    printf("Before:\n");
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            printf("%d ", A[i][j]);
            row[i] &= A[i][j];
            col[j] &= A[i][j];
        }
        printf("\n");
    }

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (!row[i] || !col[j])
                A[i][j] = 0;
        }
    }

    printf("After:\n");
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            printf("%d ", A[i][j]);
        }
        printf("\n");
    }

    return 0;
}
