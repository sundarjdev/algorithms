#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

struct node *push(struct node *head, int val)
{
    struct node *node = (struct node *)malloc(sizeof(struct node));
    node->val = val;
    node->next = head;
    return node;
}

void printlist(struct node *head)
{
    while (head) {
        printf("%d-->", head->val);
        head = head->next;
    }
    printf("\n");
}

void frontbacksplit(struct node *head, struct node **aRef, struct node **bRef)
{
    if (head == NULL) // if empty list return
        return;

    struct node *sptr = head;
    struct node *fptr = head;

    if (fptr->next == NULL) { // single element list
        *aRef = head;
        *bRef = NULL;
        return;
    }

    if (fptr->next->next == NULL) { // 2 element list
        *aRef = head;
        *bRef = head->next;
        head->next = NULL;
        return;
    }

    while (fptr->next && fptr->next->next) {
        sptr = sptr->next;
        fptr = fptr->next->next;
    }
    *aRef = head;
    *bRef = sptr->next;
    sptr->next = NULL;
    return;
}

void sortedmerge(struct node **headref, struct node *a, struct node *b)
{
    while (1) {
        if (a == NULL) {
            *headref = b;
            return;
        }

        if (b == NULL) {
            *headref = a;
            return;
        }

        if (a->val < b->val) { // take from 'a' list
            *headref = a;
            a = a->next;
            (*headref)->next = NULL;
            headref = &((*headref)->next);
        } else { // take from 'b' list
            *headref = b;
            b = b->next;
            (*headref)->next = NULL;
            headref = &((*headref)->next);
        }
    }
}

void mergesort(struct node **headref)
{
    struct node *head = *headref;

    if (head == NULL || head->next == NULL) { // if empty list list or single element list, return
        return;
    }

    struct node *a = NULL;
    struct node *b = NULL;

    frontbacksplit(head, &a, &b); // split list into 2 halves

    mergesort(&a); // recursively sort the 2 halves
    mergesort(&b);

    sortedmerge(headref, a, b); // merge the 2 sorted lists
}

int main(void)
{
    struct node *head = NULL;

    for (int i = 0; i < 10; i++)
        head = push(head, i);

    printlist(head);

    mergesort(&head);

    printlist(head);

    return 0;
}
