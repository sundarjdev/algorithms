/*
 * Stack with max
 * Create a data structure that efficiently supports the stack operations (push
 * and pop) and also a return-the-maximum operation. Assume the elements are
 * reals numbers so that you can compare them.
 *
 * to compile: javac StackWithMax.java
 * to execute: java StackWithMax
 */
import java.util.NoSuchElementException;
public class StackWithMax
{
    private Node first;

    private class Node {
        private int item;
        private int max;
        private Node next;
    }

    public StackWithMax()
    {
        first = null;
    }
    public void push(int item)
    {
        Node node = new Node();
        node.item = item;
        node.next = first;
        if (isEmpty())
            node.max = item;
        else {
            node.max = (item > first.max) ? item : first.max;
        }

        first = node;
    }
    public int pop()
    {
        if (isEmpty())
            throw new NoSuchElementException("Empty Stack"); 

        int item = first.item;
        first = first.next;
        return item;
    }
    public int getMax()
    {
        if (isEmpty())
            throw new NoSuchElementException("Empty Stack"); 
        return first.max;
    }
    public boolean isEmpty()
    {
        return (first == null);
    }

    public static void main(String[] args)
    {
        StackWithMax S = new StackWithMax();
        int item;

        item = (int) (100 * Math.random());
        StdOut.println("Pushing- " + item);
        S.push(item);
        StdOut.println("Max- " + S.getMax());

        item = (int) (100 * Math.random());
        StdOut.println("Pushing- " + item);
        S.push(item);
        StdOut.println("Max- " + S.getMax());

        item = (int) (100 * Math.random());
        StdOut.println("Pushing- " + item);
        S.push(item);
        StdOut.println("Max- " + S.getMax());

        StdOut.println("Poping- " + S.pop());
        StdOut.println("Max- " + S.getMax());
        StdOut.println("Poping- " + S.pop());
        StdOut.println("Max- " + S.getMax());
        StdOut.println("Poping- " + S.pop());
    }
}
