/*
 * Qstack with two stacks
 * Implement a queue with two stacks so that each queue operations takes a
 * constant amortized number of stack operations
 *
 * to compile: javac Qstack.java
 * to execute: java Qstack
 */
import java.util.NoSuchElementException;
public class Qstack<Item>
{
    private Stack<Item> S1, S2;

    private class Stack<Item>
    {
        private Node<Item> first;
        private class Node<Item>
        {
            private Item item;
            private Node<Item> next;
        }

        public Stack()
        {
            first = null;
        }
        public void push(Item item)
        {
            Node<Item> node = new Node<Item>();
            node.item = item;
            node.next = first;
            first = node;
        }
        public Item pop()
        {
            if (isEmpty())
                throw new NoSuchElementException("Empty Stack"); 
            Item temp = first.item;
            first = first.next;
            return temp;
        }
        public boolean isEmpty()
        {
            return (first == null);
        }
    }

    public Qstack()
    {
        S1 = new Stack<Item>();
        S2 = new Stack<Item>();
    }

    public void enqueue(Item item)
    {
        S1.push(item);
    }

    public Item dequeue()
    {
        if (S2.isEmpty()) {
            while (!S1.isEmpty()) {
                S2.push(S1.pop());
            }
        }

        return S2.pop();
    }

    public boolean isEmpty()
    {
        return (S1.isEmpty() && S2.isEmpty());
    }

    public static void main(String[] args)
    {
        Qstack<Integer> Q = new Qstack<Integer>();
        int item;

        item = (int) (100 * Math.random());
        StdOut.println("Enqueueing- " + item);
        Q.enqueue(item);
        item = (int) (100 * Math.random());
        StdOut.println("Enqueueing- " + item);
        Q.enqueue(item);
        StdOut.println("Dequeueing-" + Q.dequeue());
        item = (int) (100 * Math.random());
        StdOut.println("Enqueueing- " + item);
        Q.enqueue(item);
        item = (int) (100 * Math.random());
        StdOut.println("Enqueueing- " + item);
        Q.enqueue(item);
        StdOut.println("Dequeueing-" + Q.dequeue());
        StdOut.println("Dequeueing-" + Q.dequeue());
        StdOut.println("Dequeueing-" + Q.dequeue());
    }
}
