#include <iostream>
#include <stdio.h>
#include <cstdlib>

using namespace std;

template <class E>
class Link {
    public:
        E element;
        Link<E> *next;

    public:
        Link(E it) {
            element = it;
            next = NULL;
        }
        Link(Link<E>& nextval) {
            next = nextval;
        }
};

template <class E>
class Queue {
    public:
        virtual void clear(void) = 0;
        virtual void enqueue(E it) = 0;
        virtual E dequeue(void) = 0;
        virtual E frontValue(void) = 0;
        virtual int length(void) = 0;
};

template <class E>
class LQueue : public Queue<E> {
    private:
        Link<E> *front;
        Link<E> *rear;
        int size;

    public:
        LQueue() {
            front = rear = new Link<E>(NULL);
            size = 0;
        }
        ~LQueue() {
            delete front;
            size = 0;
        }
        void clear(void) {
            front = rear = NULL;
            size = 0;
        }
        void enqueue(E it) {
            rear->next = new Link<E>(it);
            rear = rear->next;
            size++;
        }
        E dequeue(void) {
            if (size == 0) {
                cout << "Empty queue" << endl;
                exit(1);
            }
            Link<E>* temp = front->next;
            E val = temp->element;
            front->next = temp->next;

            size--;
            delete temp;
            return val;
        }
        E frontValue(void) {
            if (size == 0) {
                cout << "Empty Queue" << endl;
                exit(1);
            }
            return front->next->element;
        }
        int length(void) {
            return size;
        }
};

int main(void)
{
    LQueue<int> q;

    for (int i = 0; i < 10; i++)
        q.enqueue(i);

    while (q.length()) {
        cout << q.dequeue() << endl;
    }

    return 0;
}
