#include <stdio.h>
#include <stdlib.h>

void insertionsort(int a[], int len)
{
    for (int i = 1; i < len; i++) {
        for (int j = 0; j < i; j++) {
            if (a[i] < a[j]) { // Swap
                a[i] = a[i] + a[j];
                a[j] = a[i] - a[j];
                a[i] = a[i] - a[j];
            }
        }
    }
}

int main(void)
{
    int a[] = {10, 1, 9, 6, 11, 15, 20, 2, 3, 5};

    printf("Before:\n");
    for (int i = 0; i < sizeof(a)/sizeof(int); i++)
        printf("%d ", a[i]);
    printf("\n");

    insertionsort(a, sizeof(a)/sizeof(int));

    printf("After:\n");
    for (int i = 0; i < sizeof(a)/sizeof(int); i++)
        printf("%d ", a[i]);
    printf("\n");

    return 0;
}
