#include <stdio.h>

void swap(int a[], int i, int j)
{
    int temp = a[i];
    a[i] = a[j];
    a[j] = temp;
}

int partition(int a[], int l, int r)
{
    int mid = (l+r)/2;
    int pivot = a[mid];

    swap(a, mid, r);
    int index = l;

    for (int i = l; i < r; i++) {
        if (a[i] <= pivot) {
            swap(a, index, i);
            index++;
        }
    }

    /* move pivot to its right place */
    swap(a, index, r);
    return index;
}

void quicksort(int a[], int l, int r)
{
    if (l >= r)
        return;

    int pivot_index = partition(a, l, r);

    quicksort(a, l, pivot_index - 1);
    quicksort(a, pivot_index + 1, r);
}

int main(void)
{
    int a[] = {7, 5, 1, 2, 3, 9, 8, 6, 4};
    int size = sizeof(a)/sizeof(int);

    for (int i = 0; i < size; i++)
        printf("%d ", a[i]);
    printf("\n");

    quicksort(a, 0, size - 1);

    for (int i = 0; i < size; i++)
        printf("%d ", a[i]);
    printf("\n");

    return 0;
}
