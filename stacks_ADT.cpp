#include <iostream>
#include <stdlib.h>

using namespace std;

template <class E>
class Stack {
    public:
        virtual void clear(void) = 0;
        virtual void push(E it) = 0;
        virtual E pop(void) = 0;
        virtual E topValue(void) = 0;
        virtual int length(void) = 0;
};

template <class E>
class AStack:public Stack<E> {
    private:
        static int defaultSize;
        int maxSize;
        int top;
        E *arrayStack;
    public:
        AStack() {
            maxSize = 10;
            top = 0;
            arrayStack = new E[10];
        }
        AStack(int size) {
            maxSize = size;
            top = 0;
            arrayStack = new E[size];
        }
        ~AStack() {
            maxSize = 0;
            top = 0;
            delete [] arrayStack;
        }
        void clear(void) {
            top = 0;
        }
        void push(E it) {
            if (top == maxSize) {
                cout << "Stack is full" << endl;
                return;
            }
            arrayStack[top++] = it;
        }
        E pop(void) {
            if (top == 0) {
                cout << "Stack is empty" << endl;
                return 0;
            }
            return arrayStack[--top];
        }
        E topValue(void) {
            if (top == 0) {
                cout << "Stack is empty" << endl;
                return 0;
            }
            return arrayStack[top-1];
        }
        int length(void) {
            return top;
        }
};

template <class E>
int AStack<E>::defaultSize = 10;

int main(void)
{
    AStack<int> s;
    for (int i = 0; i < 10; i++)
        s.push(i);

    while (s.length()) {
        cout << s.pop() << endl;
    }

    return 0;
}

