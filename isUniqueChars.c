#include <stdio.h>
#include <string.h>

enum {
    true,
    false
};

int isUniqueChars(char *a)
{
    int string[256] = {0};

    for (int i = 0; i < strlen(a); i++) {
        if (string[a[i]])
            return false;
        else
            string[a[i]] = a[i];
    }

    return true;
}

int main(void)
{
    char a[] = "Sundar";
    char b[] = "vani";
    char c[] = "sundarvani";

    printf("a: %d\n", isUniqueChars(a));
    printf("b: %d\n", isUniqueChars(b));
    printf("c: %d\n", isUniqueChars(c));

    return 0;
}
