#include <iostream>
#include <stdio.h>
#include <cstdlib>

using namespace std;

template <class E>
class Link {
    public:
        E element;
        Link<E> *next;

    public:
        Link(void) {
            element = 0;
            next = NULL;
        }
        Link(E elem) {
            element = elem;
            next = NULL;
        }
        Link(Link<E>& nextval) {
            element = 0;
            next = nextval;
        }
};

template <class E>
class Stack {
    public:
        virtual void clear(void) = 0;
        virtual void push(E elem) = 0;
        virtual E pop(void) = 0;
        virtual E topValue(void) = 0;
        virtual int length(void) = 0;
};

template <class E>
class LStack : public Stack<E> {
    private:
        Link<E> *top;
        int size;

    public:
        LStack() {
            top = NULL;
            size = 0;
        }
        ~LStack() {
            size = 0;
        }

        void clear(void) {
            top = NULL;
        }
        void push(E elem) {
            Link<E> *node = new Link<E>(elem);
            node->next = top;
            top = node;
            size++;
        }
        E pop(void) {
            if (size == 0) {
                cout << "Empty stack" << endl;
                exit(1);
            }

            Link<E> *temp = top;
            top = top->next;
            size--;

            E val = temp->element;
            delete temp;
            return val;
        }
        E topValue(void) {
            if (size == 0) {
                cout << "Empty stack" << endl;
                exit(1);
            }
            return top->element;
        }
        int length(void) {
            return size;
        }
};

int main(void)
{
    LStack<int> s;

    for (int i = 0; i < 10; i++)
        s.push(i);

    while (s.length()) {
        cout << s.pop() << endl;
    }

    return 0;
}

