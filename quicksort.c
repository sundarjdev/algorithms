#include <stdio.h>

void swap(int a[], int i, int j)
{
    int temp = a[j];
    a[j] = a[i];
    a[i] = temp;
}

int partition(int a[], int l, int r, int pivot)
{
    int pivotvalue = a[pivot];

    /* move pivot to end of array */
    swap(a, pivot, r);

    int index = l;

    for (int i = l; i < r; i++) {
        if (a[i] <= pivotvalue) {
            swap(a, index, i);
            index++;
        }
    }

    /* move pivot to its right place */
    swap(a, index, r);
    return index;
}

void quicksort(int a[], int left, int right)
{
    if (left > right)
        return;

    int pivot = partition(a, left, right, (left + right) / 2);

    quicksort(a, left, pivot-1);
    quicksort(a, pivot+1, right);
}

int main(void)
{
    //int a[] = {8, 3, 1, 9, 2};
    //int a[] = {9, 2, 13, 7, 1, 0, 15, 8, 6, 3, 4};
    int a[] = {9, 2, 13, 7, 1, 15, 8, 6, 3, 4};

    printf("Before:\n");
    for (int i = 0; i < sizeof(a)/sizeof(int); i++)
        printf("%d ", a[i]);
    printf("\n");

    quicksort(a, 0, (sizeof(a)/sizeof(int)) - 1);

    printf("After:\n");
    for (int i = 0; i < sizeof(a)/sizeof(int); i++)
        printf("%d ", a[i]);
    printf("\n");

    return 0;
}
